from flask import Flask, jsonify, request


app = Flask(__name__)


@app.route('/')
def home():
    return jsonify(data='Welcome to my MLE02 Home Page!')


if __name__ == '__main__':
    app.run()
